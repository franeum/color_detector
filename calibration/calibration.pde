int c = 0;

void setup() {
  size(1600,600);
  frameRate(0.5);
}

void draw() {
  background(0, min(c*128,255), 0);
  c++;
  if (c > 2) c = 0;
}
