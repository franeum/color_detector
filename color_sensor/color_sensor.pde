int n = 9;
int green = 0;
int[][] colors = {
{255,0,0},
{0,255,0},
{0,0,255},
{0,255,255},
{255,0,255},
{255,255,0},
{0,0,0},
{255,255,255},
{255,128,0}
};

void setup() {
  size(1900,600);
  frameRate(8);
  noStroke();
  for (int i=0; i<n; i++) {
    fill(colors[i][0],colors[i][1],colors[i][2]);
    rect(width/n*i,0,width/n,height);
  }
}

void draw() {}
