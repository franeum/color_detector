import time
import board
import digitalio
import pulseio
import simpleio
import busio
from pybox.led import LED

led = LED()
COLOR_LEVELS = {
    'RED': (False, False),
    'GREEN': (True, True),
    'BLUE': (False, True),
    'WHITE': (True, False)
}

COLOR_RANGES = {
    'RED': (120, 380),
    'GREEN': (170, 430),
    'BLUE': (160, 380),
    'WHITE': (0, 100)
}

# pins to set frequency scaling
S0 = digitalio.DigitalInOut(board.GP8)
S0.direction = digitalio.Direction.OUTPUT
S1 = digitalio.DigitalInOut(board.GP7)
S1.direction = digitalio.Direction.OUTPUT

# pins to set color to detect
S2 = digitalio.DigitalInOut(board.GP6)
S2.direction = digitalio.Direction.OUTPUT
S3 = digitalio.DigitalInOut(board.GP5)
S3.direction = digitalio.Direction.OUTPUT

# pin to read frequency
OUT = pulseio.PulseIn(board.GP4, idle_state=True)

# pin to activate device (preset only in som models)
OE = digitalio.DigitalInOut(board.GP3)
OE.direction = digitalio.Direction.OUTPUT


# Set output frequency scaling to 2%
S0.value = True
S1.value = False

# activate device
OE.value = False

color = "NONE"
prev_color = ""

def detect_color(color):
    S2.value, S3.value = COLOR_LEVELS[color]
    min_in, max_in = COLOR_RANGES[color]

    time.sleep(0.005)
    
    while len(OUT) == 0:
        pass

    OUT.pause()
    freq = OUT[0]
    color = simpleio.map_range(freq, min_in, max_in, 1, 0) 
    OUT.clear()
    OUT.resume()

    return int(color**2 * 255), freq


while True:
    red, red_freq = detect_color('RED')
    green, green_freq = detect_color('GREEN')
    blue, blue_freq = detect_color('BLUE')
    white, white_freq = detect_color('WHITE')
    
    #print(red_freq, green_freq, blue_freq)
    print(red, green, blue)
    #print(white_freq)
    
    red = 255 if red > 200 else 0
    green = 255 if green > 200 else 0
    blue = 255 if blue > 200 else 0
    #print(red, green, blue)
    
    led.color = (red, green, blue)
    led.on()
    #print(f"RED: {green_freq}")   

    time.sleep(0.1)

    OUT.clear()
    OUT.resume()
